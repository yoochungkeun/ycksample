package yck.searchresultlist.databinding;
import yck.searchresultlist.R;
import yck.searchresultlist.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainDetailReviewBindingImpl extends ActivityMainDetailReviewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_detail_review_rate_total_title, 6);
        sViewsWithIds.put(R.id.tv_detail_review_pros_title, 7);
        sViewsWithIds.put(R.id.tv_detail_review_cons_title, 8);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainDetailReviewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private ActivityMainDetailReviewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[3]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvDetailReviewCons.setTag(null);
        this.tvDetailReviewName.setTag(null);
        this.tvDetailReviewPros.setTag(null);
        this.tvDetailReviewRateTotal.setTag(null);
        this.tvDetailReviewReview.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemsDetailData == variableId) {
            setItemsDetailData((yck.searchresultlist.model.ResItemsDetailData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemsDetailData(@Nullable yck.searchresultlist.model.ResItemsDetailData ItemsDetailData) {
        this.mItemsDetailData = ItemsDetailData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemsDetailData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemsDetailDataRateTotalAvg = null;
        yck.searchresultlist.model.ResItemsDetailData itemsDetailData = mItemsDetailData;
        java.lang.String itemsDetailDataPros = null;
        java.lang.String itemsDetailDataName = null;
        java.lang.String itemsDetailDataCons = null;
        java.lang.String itemsDetailDataReviewSummary = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (itemsDetailData != null) {
                    // read itemsDetailData.rate_total_avg
                    itemsDetailDataRateTotalAvg = itemsDetailData.getRate_total_avg();
                    // read itemsDetailData.pros
                    itemsDetailDataPros = itemsDetailData.getPros();
                    // read itemsDetailData.name
                    itemsDetailDataName = itemsDetailData.getName();
                    // read itemsDetailData.cons
                    itemsDetailDataCons = itemsDetailData.getCons();
                    // read itemsDetailData.review_summary
                    itemsDetailDataReviewSummary = itemsDetailData.getReview_summary();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailReviewCons, itemsDetailDataCons);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailReviewName, itemsDetailDataName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailReviewPros, itemsDetailDataPros);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailReviewRateTotal, itemsDetailDataRateTotalAvg);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailReviewReview, itemsDetailDataReviewSummary);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemsDetailData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}