package yck.searchresultlist.databinding;
import yck.searchresultlist.R;
import yck.searchresultlist.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CellTypeHorizontalBindingImpl extends CellTypeHorizontalBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.contr_top_type_company, 3);
        sViewsWithIds.put(R.id.ll_line_type_hori, 4);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public CellTypeHorizontalBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private CellTypeHorizontalBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (android.widget.ImageView) bindings[1]
            , (android.view.View) bindings[4]
            , (android.widget.TextView) bindings[2]
            );
        this.ivCoverImageTypeHori.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvTitleTypeHori.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemsDetailDataTheme == variableId) {
            setItemsDetailDataTheme((yck.searchresultlist.model.ResItemsDetailDataTheme) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemsDetailDataTheme(@Nullable yck.searchresultlist.model.ResItemsDetailDataTheme ItemsDetailDataTheme) {
        this.mItemsDetailDataTheme = ItemsDetailDataTheme;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemsDetailDataTheme);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        yck.searchresultlist.model.ResItemsDetailDataTheme itemsDetailDataTheme = mItemsDetailDataTheme;
        java.lang.String itemsDetailDataThemeTitle = null;
        java.lang.String itemsDetailDataThemeCoverImage = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (itemsDetailDataTheme != null) {
                    // read itemsDetailDataTheme.title
                    itemsDetailDataThemeTitle = itemsDetailDataTheme.getTitle();
                    // read itemsDetailDataTheme.cover_image
                    itemsDetailDataThemeCoverImage = itemsDetailDataTheme.getCover_image();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            yck.searchresultlist.conversion.BindingConversions.loadImage(this.ivCoverImageTypeHori, itemsDetailDataThemeCoverImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTitleTypeHori, itemsDetailDataThemeTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemsDetailDataTheme
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}