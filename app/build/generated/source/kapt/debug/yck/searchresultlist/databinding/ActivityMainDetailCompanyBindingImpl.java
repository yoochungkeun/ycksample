package yck.searchresultlist.databinding;
import yck.searchresultlist.R;
import yck.searchresultlist.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainDetailCompanyBindingImpl extends ActivityMainDetailCompanyBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.tv_detail_company_rate_total_title, 7);
        sViewsWithIds.put(R.id.tv_detail_company_salary_avg_title, 8);
        sViewsWithIds.put(R.id.tv_detail_company_salary_avg_2, 9);
        sViewsWithIds.put(R.id.tv_detail_company_question_title, 10);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainDetailCompanyBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private ActivityMainDetailCompanyBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[1]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[8]
            );
        this.ivDetailCompanyLogo.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvDetailCompanyIndustry.setTag(null);
        this.tvDetailCompanyName.setTag(null);
        this.tvDetailCompanyQuestion.setTag(null);
        this.tvDetailCompanyRateTotal.setTag(null);
        this.tvDetailCompanySalaryAvg.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemsDetailData == variableId) {
            setItemsDetailData((yck.searchresultlist.model.ResItemsDetailData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemsDetailData(@Nullable yck.searchresultlist.model.ResItemsDetailData ItemsDetailData) {
        this.mItemsDetailData = ItemsDetailData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemsDetailData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemsDetailDataInterviewQuestion = null;
        java.lang.String itemsDetailDataRateTotalAvg = null;
        yck.searchresultlist.model.ResItemsDetailData itemsDetailData = mItemsDetailData;
        java.lang.String itemsDetailDataLogoPath = null;
        java.lang.String itemsDetailDataIndustryName = null;
        java.lang.String itemsDetailDataSalaryAvg = null;
        java.lang.String itemsDetailDataName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (itemsDetailData != null) {
                    // read itemsDetailData.interview_question
                    itemsDetailDataInterviewQuestion = itemsDetailData.getInterview_question();
                    // read itemsDetailData.rate_total_avg
                    itemsDetailDataRateTotalAvg = itemsDetailData.getRate_total_avg();
                    // read itemsDetailData.logo_path
                    itemsDetailDataLogoPath = itemsDetailData.getLogo_path();
                    // read itemsDetailData.industry_name
                    itemsDetailDataIndustryName = itemsDetailData.getIndustry_name();
                    // read itemsDetailData.salary_avg
                    itemsDetailDataSalaryAvg = itemsDetailData.getSalary_avg();
                    // read itemsDetailData.name
                    itemsDetailDataName = itemsDetailData.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            yck.searchresultlist.conversion.BindingConversions.loadImage(this.ivDetailCompanyLogo, itemsDetailDataLogoPath);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailCompanyIndustry, itemsDetailDataIndustryName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailCompanyName, itemsDetailDataName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailCompanyQuestion, itemsDetailDataInterviewQuestion);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDetailCompanyRateTotal, itemsDetailDataRateTotalAvg);
            yck.searchresultlist.conversion.BindingConversions.convertDecimalNum(this.tvDetailCompanySalaryAvg, itemsDetailDataSalaryAvg);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemsDetailData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}