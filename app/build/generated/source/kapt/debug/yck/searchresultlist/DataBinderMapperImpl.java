package yck.searchresultlist;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import yck.searchresultlist.databinding.ActivityMainDetailCompanyBindingImpl;
import yck.searchresultlist.databinding.ActivityMainDetailReviewBindingImpl;
import yck.searchresultlist.databinding.CellTypeCompanyBindingImpl;
import yck.searchresultlist.databinding.CellTypeDefaultBindingImpl;
import yck.searchresultlist.databinding.CellTypeHorizontalBindingImpl;
import yck.searchresultlist.databinding.CellTypeReviewBindingImpl;
import yck.searchresultlist.databinding.RvHorizontalBindingImpl;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAINDETAILCOMPANY = 1;

  private static final int LAYOUT_ACTIVITYMAINDETAILREVIEW = 2;

  private static final int LAYOUT_CELLTYPECOMPANY = 3;

  private static final int LAYOUT_CELLTYPEDEFAULT = 4;

  private static final int LAYOUT_CELLTYPEHORIZONTAL = 5;

  private static final int LAYOUT_CELLTYPEREVIEW = 6;

  private static final int LAYOUT_RVHORIZONTAL = 7;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(7);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.activity_main_detail_company, LAYOUT_ACTIVITYMAINDETAILCOMPANY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.activity_main_detail_review, LAYOUT_ACTIVITYMAINDETAILREVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.cell_type_company, LAYOUT_CELLTYPECOMPANY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.cell_type_default, LAYOUT_CELLTYPEDEFAULT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.cell_type_horizontal, LAYOUT_CELLTYPEHORIZONTAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.cell_type_review, LAYOUT_CELLTYPEREVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(yck.searchresultlist.R.layout.rv_horizontal, LAYOUT_RVHORIZONTAL);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAINDETAILCOMPANY: {
          if ("layout/activity_main_detail_company_0".equals(tag)) {
            return new ActivityMainDetailCompanyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main_detail_company is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYMAINDETAILREVIEW: {
          if ("layout/activity_main_detail_review_0".equals(tag)) {
            return new ActivityMainDetailReviewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main_detail_review is invalid. Received: " + tag);
        }
        case  LAYOUT_CELLTYPECOMPANY: {
          if ("layout/cell_type_company_0".equals(tag)) {
            return new CellTypeCompanyBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for cell_type_company is invalid. Received: " + tag);
        }
        case  LAYOUT_CELLTYPEDEFAULT: {
          if ("layout/cell_type_default_0".equals(tag)) {
            return new CellTypeDefaultBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for cell_type_default is invalid. Received: " + tag);
        }
        case  LAYOUT_CELLTYPEHORIZONTAL: {
          if ("layout/cell_type_horizontal_0".equals(tag)) {
            return new CellTypeHorizontalBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for cell_type_horizontal is invalid. Received: " + tag);
        }
        case  LAYOUT_CELLTYPEREVIEW: {
          if ("layout/cell_type_review_0".equals(tag)) {
            return new CellTypeReviewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for cell_type_review is invalid. Received: " + tag);
        }
        case  LAYOUT_RVHORIZONTAL: {
          if ("layout/rv_horizontal_0".equals(tag)) {
            return new RvHorizontalBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for rv_horizontal is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(3);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "itemsDetailData");
      sKeys.put(2, "itemsDetailDataTheme");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(7);

    static {
      sKeys.put("layout/activity_main_detail_company_0", yck.searchresultlist.R.layout.activity_main_detail_company);
      sKeys.put("layout/activity_main_detail_review_0", yck.searchresultlist.R.layout.activity_main_detail_review);
      sKeys.put("layout/cell_type_company_0", yck.searchresultlist.R.layout.cell_type_company);
      sKeys.put("layout/cell_type_default_0", yck.searchresultlist.R.layout.cell_type_default);
      sKeys.put("layout/cell_type_horizontal_0", yck.searchresultlist.R.layout.cell_type_horizontal);
      sKeys.put("layout/cell_type_review_0", yck.searchresultlist.R.layout.cell_type_review);
      sKeys.put("layout/rv_horizontal_0", yck.searchresultlist.R.layout.rv_horizontal);
    }
  }
}
