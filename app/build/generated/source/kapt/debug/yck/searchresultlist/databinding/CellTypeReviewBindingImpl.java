package yck.searchresultlist.databinding;
import yck.searchresultlist.R;
import yck.searchresultlist.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CellTypeReviewBindingImpl extends CellTypeReviewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.contr_top_type_review, 8);
        sViewsWithIds.put(R.id.vw_line, 9);
        sViewsWithIds.put(R.id.contr_body_type_review, 10);
        sViewsWithIds.put(R.id.contr_pros_type_review, 11);
        sViewsWithIds.put(R.id.tv_pros_type_review_title, 12);
        sViewsWithIds.put(R.id.contr_cons_type_review, 13);
        sViewsWithIds.put(R.id.tv_cons_type_review_title, 14);
        sViewsWithIds.put(R.id.tv_more_type_review, 15);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public CellTypeReviewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private CellTypeReviewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[10]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[8]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[5]
            , (android.view.View) bindings[9]
            );
        this.ivLogoPathTypeReview.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvConsTypeReview.setTag(null);
        this.tvIndustryNameTypeReview.setTag(null);
        this.tvNameTypeReview.setTag(null);
        this.tvProsTypeReview.setTag(null);
        this.tvRateTotalAvgTypeReview.setTag(null);
        this.tvReviewSummaryTypeReview.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.itemsDetailData == variableId) {
            setItemsDetailData((yck.searchresultlist.model.ResItemsDetailData) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setItemsDetailData(@Nullable yck.searchresultlist.model.ResItemsDetailData ItemsDetailData) {
        this.mItemsDetailData = ItemsDetailData;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.itemsDetailData);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String itemsDetailDataRateTotalAvg = null;
        yck.searchresultlist.model.ResItemsDetailData itemsDetailData = mItemsDetailData;
        java.lang.String itemsDetailDataLogoPath = null;
        java.lang.String itemsDetailDataPros = null;
        java.lang.String itemsDetailDataIndustryName = null;
        java.lang.String itemsDetailDataName = null;
        java.lang.String itemsDetailDataCons = null;
        java.lang.String itemsDetailDataReviewSummary = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (itemsDetailData != null) {
                    // read itemsDetailData.rate_total_avg
                    itemsDetailDataRateTotalAvg = itemsDetailData.getRate_total_avg();
                    // read itemsDetailData.logo_path
                    itemsDetailDataLogoPath = itemsDetailData.getLogo_path();
                    // read itemsDetailData.pros
                    itemsDetailDataPros = itemsDetailData.getPros();
                    // read itemsDetailData.industry_name
                    itemsDetailDataIndustryName = itemsDetailData.getIndustry_name();
                    // read itemsDetailData.name
                    itemsDetailDataName = itemsDetailData.getName();
                    // read itemsDetailData.cons
                    itemsDetailDataCons = itemsDetailData.getCons();
                    // read itemsDetailData.review_summary
                    itemsDetailDataReviewSummary = itemsDetailData.getReview_summary();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            yck.searchresultlist.conversion.BindingConversions.loadImage(this.ivLogoPathTypeReview, itemsDetailDataLogoPath);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvConsTypeReview, itemsDetailDataCons);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvIndustryNameTypeReview, itemsDetailDataIndustryName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvNameTypeReview, itemsDetailDataName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProsTypeReview, itemsDetailDataPros);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvRateTotalAvgTypeReview, itemsDetailDataRateTotalAvg);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvReviewSummaryTypeReview, itemsDetailDataReviewSummary);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): itemsDetailData
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}