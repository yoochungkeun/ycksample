package yck.searchresultlist.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0005\"\u001d\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0005\",\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"commonutilModule", "Lkotlin/Function1;", "Lorg/koin/core/KoinContext;", "Lorg/koin/dsl/context/ModuleDefinition;", "getCommonutilModule", "()Lkotlin/jvm/functions/Function1;", "customDImodules", "", "getCustomDImodules", "()Ljava/util/List;", "setCustomDImodules", "(Ljava/util/List;)V", "app_debug"})
public final class DIModulesKt {
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.jvm.functions.Function1<org.koin.core.KoinContext, org.koin.dsl.context.ModuleDefinition> commonutilModule = null;
    @org.jetbrains.annotations.NotNull()
    private static java.util.List<? extends kotlin.jvm.functions.Function1<? super org.koin.core.KoinContext, org.koin.dsl.context.ModuleDefinition>> customDImodules;
    
    @org.jetbrains.annotations.NotNull()
    public static final kotlin.jvm.functions.Function1<org.koin.core.KoinContext, org.koin.dsl.context.ModuleDefinition> getCommonutilModule() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<kotlin.jvm.functions.Function1<org.koin.core.KoinContext, org.koin.dsl.context.ModuleDefinition>> getCustomDImodules() {
        return null;
    }
    
    public static final void setCustomDImodules(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends kotlin.jvm.functions.Function1<? super org.koin.core.KoinContext, org.koin.dsl.context.ModuleDefinition>> p0) {
    }
}