package yck.searchresultlist.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lyck/searchresultlist/base/BaseConstant;", "", "()V", "BASEURL", "", "CELL_TYPE_COMPANY", "CELL_TYPE_HORIZONTAL_THEME", "CELL_TYPE_REVIEW", "PUT_RESINFO_DETAIL_DATA_", "app_debug"})
public final class BaseConstant {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASEURL = "https://jpassets.jobplanet.co.kr/mobile-config/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CELL_TYPE_REVIEW = "CELL_TYPE_REVIEW";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CELL_TYPE_COMPANY = "CELL_TYPE_COMPANY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CELL_TYPE_HORIZONTAL_THEME = "CELL_TYPE_HORIZONTAL_THEME";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PUT_RESINFO_DETAIL_DATA_ = "resInfo_detail";
    public static final yck.searchresultlist.base.BaseConstant INSTANCE = null;
    
    private BaseConstant() {
        super();
    }
}