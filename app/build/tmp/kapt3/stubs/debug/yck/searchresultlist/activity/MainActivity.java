package yck.searchresultlist.activity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\bJ\u0012\u0010\f\u001a\u00020\b2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\b\u0010\u000f\u001a\u00020\bH\u0014J\u0006\u0010\u0010\u001a\u00020\bJ\u0006\u0010\u0011\u001a\u00020\bJ\u000e\u0010\u0012\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lyck/searchresultlist/activity/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "mMainViewModel", "Lyck/searchresultlist/viewmodel/MainViewModel;", "main_adapter", "Lyck/searchresultlist/adapter/MainRvAdapter;", "callbackFromViewModel", "", "any", "", "obServerTEST", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onItemClickListener", "registerObserver", "setAdpater", "data", "Lyck/searchresultlist/model/ResItemsData;", "unregisterObserver", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity {
    private final yck.searchresultlist.viewmodel.MainViewModel mMainViewModel = null;
    private yck.searchresultlist.adapter.MainRvAdapter main_adapter;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void obServerTEST() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    public final void registerObserver() {
    }
    
    public final void unregisterObserver() {
    }
    
    public final void callbackFromViewModel(@org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public final void setAdpater(@org.jetbrains.annotations.NotNull()
    yck.searchresultlist.model.ResItemsData data) {
    }
    
    public final void onItemClickListener() {
    }
    
    public MainActivity() {
        super();
    }
}