package yck.searchresultlist.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\n\u001a\u00020\u000bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u00a8\u0006\f"}, d2 = {"Lyck/searchresultlist/viewmodel/MainViewModel;", "Lyck/searchresultlist/base/BaseViewModel;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "mLoginLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lyck/searchresultlist/model/ResItemsData;", "getMLoginLiveData", "()Landroidx/lifecycle/MutableLiveData;", "getJsonInfoData", "", "app_debug"})
public final class MainViewModel extends yck.searchresultlist.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<yck.searchresultlist.model.ResItemsData> mLoginLiveData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<yck.searchresultlist.model.ResItemsData> getMLoginLiveData() {
        return null;
    }
    
    public final void getJsonInfoData() {
    }
    
    public MainViewModel(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}