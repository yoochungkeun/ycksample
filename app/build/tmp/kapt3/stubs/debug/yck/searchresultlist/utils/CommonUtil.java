package yck.searchresultlist.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\tR\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lyck/searchresultlist/utils/CommonUtil;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "mContext", "isEmptyString", "", "source", "", "app_debug"})
public final class CommonUtil {
    private android.content.Context mContext;
    
    public final boolean isEmptyString(@org.jetbrains.annotations.Nullable()
    java.lang.String source) {
        return false;
    }
    
    public CommonUtil(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}