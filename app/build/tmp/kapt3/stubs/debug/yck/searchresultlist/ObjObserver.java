package yck.searchresultlist;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lyck/searchresultlist/ObjObserver;", "", "()V", "observer", "Lio/reactivex/Observer;", "getObserver", "()Lio/reactivex/Observer;", "app_debug"})
public final class ObjObserver {
    @org.jetbrains.annotations.NotNull()
    private static final io.reactivex.Observer<java.lang.Object> observer = null;
    public static final yck.searchresultlist.ObjObserver INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Observer<java.lang.Object> getObserver() {
        return null;
    }
    
    private ObjObserver() {
        super();
    }
}