package yck.searchresultlist.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0086\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0016\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000b\u00a2\u0006\u0002\u0010\fJ\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003J\u0019\u0010!\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000bH\u00c6\u0003JK\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\u0018\b\u0002\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000bH\u00c6\u0001J\u0013\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&H\u00d6\u0003J\t\u0010\'\u001a\u00020\u0005H\u00d6\u0001J\t\u0010(\u001a\u00020\u0003H\u00d6\u0001R*\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0016\"\u0004\b\u001a\u0010\u0018R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u0016\"\u0004\b\u001c\u0010\u0018\u00a8\u0006)"}, d2 = {"Lyck/searchresultlist/model/ResItemsData;", "Ljava/io/Serializable;", "minimum_interviews", "", "total_page", "", "minimum_reviews", "total_count", "items", "Ljava/util/ArrayList;", "Lyck/searchresultlist/model/ResItemsDetailData;", "Lkotlin/collections/ArrayList;", "(Ljava/lang/String;IIILjava/util/ArrayList;)V", "getItems", "()Ljava/util/ArrayList;", "setItems", "(Ljava/util/ArrayList;)V", "getMinimum_interviews", "()Ljava/lang/String;", "setMinimum_interviews", "(Ljava/lang/String;)V", "getMinimum_reviews", "()I", "setMinimum_reviews", "(I)V", "getTotal_count", "setTotal_count", "getTotal_page", "setTotal_page", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "", "hashCode", "toString", "app_debug"})
public final class ResItemsData implements java.io.Serializable {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String minimum_interviews;
    private int total_page;
    private int minimum_reviews;
    private int total_count;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<yck.searchresultlist.model.ResItemsDetailData> items;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMinimum_interviews() {
        return null;
    }
    
    public final void setMinimum_interviews(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int getTotal_page() {
        return 0;
    }
    
    public final void setTotal_page(int p0) {
    }
    
    public final int getMinimum_reviews() {
        return 0;
    }
    
    public final void setMinimum_reviews(int p0) {
    }
    
    public final int getTotal_count() {
        return 0;
    }
    
    public final void setTotal_count(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<yck.searchresultlist.model.ResItemsDetailData> getItems() {
        return null;
    }
    
    public final void setItems(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<yck.searchresultlist.model.ResItemsDetailData> p0) {
    }
    
    public ResItemsData(@org.jetbrains.annotations.NotNull()
    java.lang.String minimum_interviews, int total_page, int minimum_reviews, int total_count, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<yck.searchresultlist.model.ResItemsDetailData> items) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    public final int component2() {
        return 0;
    }
    
    public final int component3() {
        return 0;
    }
    
    public final int component4() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<yck.searchresultlist.model.ResItemsDetailData> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final yck.searchresultlist.model.ResItemsData copy(@org.jetbrains.annotations.NotNull()
    java.lang.String minimum_interviews, int total_page, int minimum_reviews, int total_count, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<yck.searchresultlist.model.ResItemsDetailData> items) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}