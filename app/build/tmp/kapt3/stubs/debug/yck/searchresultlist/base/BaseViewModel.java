package yck.searchresultlist.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\u0004H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\n"}, d2 = {"Lyck/searchresultlist/base/BaseViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "mIServiceAPI", "Lyck/searchresultlist/interfaces/IServiceAPI;", "getMIServiceAPI", "()Lyck/searchresultlist/interfaces/IServiceAPI;", "setMIServiceAPI", "(Lyck/searchresultlist/interfaces/IServiceAPI;)V", "getServiceAPI", "app_debug"})
public class BaseViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    public yck.searchresultlist.interfaces.IServiceAPI mIServiceAPI;
    
    @org.jetbrains.annotations.NotNull()
    public final yck.searchresultlist.interfaces.IServiceAPI getMIServiceAPI() {
        return null;
    }
    
    public final void setMIServiceAPI(@org.jetbrains.annotations.NotNull()
    yck.searchresultlist.interfaces.IServiceAPI p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public yck.searchresultlist.interfaces.IServiceAPI getServiceAPI() {
        return null;
    }
    
    public BaseViewModel() {
        super();
    }
}