package yck.searchresultlist.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000b\u0010\f\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\rR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lyck/searchresultlist/network/YCKClient;", "T", "", "()V", "mClient", "Lretrofit2/Retrofit;", "getMClient", "()Lretrofit2/Retrofit;", "setMClient", "(Lretrofit2/Retrofit;)V", "mService", "Ljava/lang/Object;", "getClient", "()Ljava/lang/Object;", "app_debug"})
public final class YCKClient<T extends java.lang.Object> {
    @org.jetbrains.annotations.NotNull()
    public retrofit2.Retrofit mClient;
    private T mService;
    
    @org.jetbrains.annotations.NotNull()
    public final retrofit2.Retrofit getMClient() {
        return null;
    }
    
    public final void setMClient(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit p0) {
    }
    
    public final T getClient() {
        return null;
    }
    
    public YCKClient() {
        super();
    }
}