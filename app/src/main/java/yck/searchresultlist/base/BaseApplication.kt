package yck.searchresultlist.base

import android.app.Application
import org.koin.android.ext.android.startKoin
import yck.searchresultlist.di.customDImodules


open class BaseApplication : Application(){

    override fun onCreate() {
        super.onCreate()
        startKoin(applicationContext,
            customDImodules
        )
    }

}//class end