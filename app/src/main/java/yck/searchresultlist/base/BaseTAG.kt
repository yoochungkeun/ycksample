package yck.searchresultlist.base

object BaseTAG {

    const val LOG_EXCEPTION = "exception"
    const val LOG_MAINACTIVITY = "MainActivity"
    const val LOG_MAINVIEWMODEL= "MainViewModel"

}