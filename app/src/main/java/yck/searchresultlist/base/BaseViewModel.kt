package yck.searchresultlist.base

import androidx.lifecycle.ViewModel
import yck.searchresultlist.interfaces.IServiceAPI
import yck.searchresultlist.network.YCKClient
import java.lang.Exception
import java.lang.reflect.Type

open class BaseViewModel:ViewModel() {

    lateinit var mIServiceAPI: IServiceAPI

    open fun getServiceAPI(): IServiceAPI {
        if(!::mIServiceAPI.isInitialized){
            try {
                if(YCKClient<Type>().getClient()==null) return null!!
                mIServiceAPI = YCKClient<Type>()
                    .getClient() as IServiceAPI
            }catch (e: Exception){
                return null!!
            }
        }
        return mIServiceAPI!!
    }


}//class end