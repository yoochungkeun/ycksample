package yck.searchresultlist.base

import android.view.LayoutInflater
import yck.searchresultlist.R


object BaseConstant {

    const val BASEURL = "https://jpassets.jobplanet.co.kr/mobile-config/"
    const val CELL_TYPE_REVIEW = "CELL_TYPE_REVIEW"
    const val CELL_TYPE_COMPANY = "CELL_TYPE_COMPANY"
    const val CELL_TYPE_HORIZONTAL_THEME = "CELL_TYPE_HORIZONTAL_THEME"
    const val PUT_RESINFO_DETAIL_DATA_ = "resInfo_detail"


}//obj end