package yck.searchresultlist.model

import java.io.Serializable


data class ResItemsData(
    var minimum_interviews : String,
    var total_page : Int,
    var minimum_reviews : Int,
    var total_count : Int,
    var items : ArrayList<ResItemsDetailData>
):Serializable

data class ResItemsDetailData(
    val ranking: Int,
    val count: String,
    var themes : ArrayList<ResItemsDetailDataTheme>,
    val cell_type: String,
    var name: String,
    val pros: String,
    val cons: String,
    val salary_avg: String,
    val web_site: String,
    var logo_path: String,
    val interview_question: String,
    val company_id: String,
    val has_job_posting: String,
    val rate_total_avg: String,
    val industry_id: String,
    val review_summary: String,
    val type: String,
    val industry_name: String,
    val simple_desc: String
):Serializable

data class ResItemsDetailDataTheme (
    val color: String,
    val cover_image: String,
    val id: Int,
    val title: String
):Serializable



