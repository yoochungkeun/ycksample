package yck.searchresultlist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import yck.searchresultlist.databinding.CellTypeHorizontalBinding
import yck.searchresultlist.interfaces.IOnItemClickListener
import yck.searchresultlist.model.ResItemsDetailDataTheme


class MainHorizontalAdapter(val context: Context, val resInfoList: ArrayList<ResItemsDetailDataTheme>) :
    RecyclerView.Adapter<MainHorizontalAdapter.Holder>() {

    private lateinit var mView:View
    private lateinit var mListener : IOnItemClickListener
    private lateinit var mResInfo_detail:ResItemsDetailDataTheme

    override fun getItemViewType(position: Int): Int {
        mResInfo_detail = resInfoList[position]
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return resInfoList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        var binding = CellTypeHorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder?.bind()
    }

    inner class Holder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        var inner_binding = binding.let {
            it
        }
        fun bind() {
            if(inner_binding==null) return
            var bindTypeHorizontal = inner_binding as CellTypeHorizontalBinding
            bindTypeHorizontal.itemsDetailDataTheme = mResInfo_detail
        }
    }

}//class end