package yck.searchresultlist.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import yck.searchresultlist.base.BaseConstant
import yck.searchresultlist.databinding.*
import yck.searchresultlist.interfaces.IOnItemClickListener
import yck.searchresultlist.model.ResItemsDetailData


class MainRvAdapter(val context: Context, val resInfoList: ArrayList<ResItemsDetailData>) :
    RecyclerView.Adapter<MainRvAdapter.Holder>() {

    private lateinit var mResInfo_detail: ResItemsDetailData
    private lateinit var mListener: IOnItemClickListener

    override fun getItemViewType(position: Int): Int {
        mResInfo_detail = resInfoList[position]
        return position
    }

    override fun getItemCount(): Int {
        return resInfoList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        if (mResInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_REVIEW)) {
            return Holder(CellTypeReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }else if (mResInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_COMPANY)) {
            return Holder(CellTypeCompanyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }else if (mResInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_HORIZONTAL_THEME)) {
            return Holder(RvHorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }else{
            return Holder(CellTypeDefaultBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind()
    }

    inner class Holder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        var inner_binding = binding.let {
            it
        }

        fun bind() {
            if(inner_binding==null) return
            if (mResInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_REVIEW)) {
                (inner_binding as CellTypeReviewBinding).itemsDetailData = mResInfo_detail
                (inner_binding as CellTypeReviewBinding).tvMoreTypeReview.setOnClickListener{
                    mListener.onItemClick(resInfoList[adapterPosition])
                }
            }else if (mResInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_COMPANY)) {
                (inner_binding as CellTypeCompanyBinding).itemsDetailData = mResInfo_detail
                (inner_binding as CellTypeCompanyBinding).tvMoreTypeCompany.setOnClickListener{
                    mListener.onItemClick(resInfoList[adapterPosition])
                }
            }else if (mResInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_HORIZONTAL_THEME)) {
                setHorizontalAdapter()
            }else{
                //other type
            }
        }

        fun setHorizontalAdapter(){
            var rv = (inner_binding as RvHorizontalBinding).rvRecyclerViewHorizontal
            rv?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rv?.setHasFixedSize(false)
            rv?.adapter = MainHorizontalAdapter(context, mResInfo_detail.themes)
        }

    }

    fun setOnItemClickListener(listener: IOnItemClickListener) {
        this.mListener = listener
    }

}//class end