package yck.searchresultlist

import android.util.Log
import io.reactivex.disposables.Disposable

object ObjObserver {

    val observer : io.reactivex.Observer<Any> = object: io.reactivex.Observer<Any> {
        override fun onComplete() {
            Log.d("ydydy","onComplete()")
        }
        override fun onSubscribe(d: Disposable) {
            Log.d("ydydy","onSubscribe() / d : ${d}")
        }
        override fun onNext(t: Any) {
            Log.d("ydydy","onNext() / t : ${t}")
        }
        override fun onError(e: Throwable) {
            Log.e("ydydy","onError() / e : ${e.message}")
        }
    }

}//class end