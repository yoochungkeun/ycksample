package yck.searchresultlist.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import yck.searchresultlist.ObjObserver
import yck.searchresultlist.viewmodel.MainViewModel
import yck.searchresultlist.R
import yck.searchresultlist.adapter.MainRvAdapter
import yck.searchresultlist.base.BaseConstant
import yck.searchresultlist.interfaces.IOnItemClickListener
import yck.searchresultlist.model.ResItemsData
import yck.searchresultlist.model.ResItemsDetailData
import yck.searchresultlist.utils.CommonUtil
import java.util.concurrent.Callable

class MainActivity : AppCompatActivity() {

    private val mMainViewModel = MainViewModel(this)
    private lateinit var main_adapter:MainRvAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        registerObserver()

         //서버 통신
         mMainViewModel.getJsonInfoData()

         //obServerTEST()
    }


    fun obServerTEST(){//아래와 같이 List Data 를 사용할때 적용시킬수도 있을듯함
        //iterator 안에 if문 만들어서 사용하는 것을 좀 더 전문화 시킨 버전인듯함
        val list = listOf(1, 2, 3, -1)//target 값을
        val listOb = Observable.fromIterable(list)//Observable 로 observer 적용 준비 상태로 만든다
        val call = Callable<Int> { 4 }//target 값을
        val callOb = Observable.fromCallable(call)//Observable 로 observer 적용 준비 상태로 만든다
        //observer 준비된 target 값에 callback 함수 등록시키기
        listOb.subscribe(ObjObserver.observer)
        //observer 준비된 target 값에 callback 함수 등록시키기
        callOb.subscribe(ObjObserver.observer)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterObserver()
    }

    fun registerObserver() {
        mMainViewModel?.mLoginLiveData?.observe(this, Observer {
            callbackFromViewModel(it)
        })
    }

    fun unregisterObserver() {
        mMainViewModel?.mLoginLiveData.removeObservers(this)
    }

    fun callbackFromViewModel(any: Any) {
        if(any is ResItemsData){
            var data = any
            setAdpater(data)
            onItemClickListener()
        }else{
            //no data
        }
    }


    fun setAdpater(data:ResItemsData){
        main_adapter = data.items?.let {
            MainRvAdapter(this, data.items)
        }
        main_recyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        main_recyclerView.setHasFixedSize(false)
        main_recyclerView.adapter = main_adapter
    }

    fun onItemClickListener(){
        main_adapter?.setOnItemClickListener(object :IOnItemClickListener{
            override fun onItemClick(resInfo_detail: ResItemsDetailData) {
                var intent = Intent(this@MainActivity, MainDetailActivity::class.java)
                intent?.putExtra(BaseConstant.PUT_RESINFO_DETAIL_DATA_,resInfo_detail)
                startActivity(intent)
            }
        })
    }


}//class end