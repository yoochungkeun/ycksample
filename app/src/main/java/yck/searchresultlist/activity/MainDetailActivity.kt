package yck.searchresultlist.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_main_detail_review.*
import yck.searchresultlist.R
import yck.searchresultlist.base.BaseConstant
import yck.searchresultlist.databinding.ActivityMainDetailCompanyBinding
import yck.searchresultlist.databinding.ActivityMainDetailReviewBinding
import yck.searchresultlist.model.ResItemsDetailData
import java.io.Serializable

class MainDetailActivity:AppCompatActivity(), Serializable {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(intent.hasExtra(BaseConstant.PUT_RESINFO_DETAIL_DATA_)){
            val resInfo_detail = intent.getSerializableExtra(BaseConstant.PUT_RESINFO_DETAIL_DATA_) as ResItemsDetailData
            if(resInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_REVIEW)){
                (DataBindingUtil.setContentView(this,R.layout.activity_main_detail_review) as ActivityMainDetailReviewBinding).itemsDetailData = resInfo_detail
            }else if(resInfo_detail.cell_type.equals(BaseConstant.CELL_TYPE_COMPANY)){
                (DataBindingUtil.setContentView(this,R.layout.activity_main_detail_company) as ActivityMainDetailCompanyBinding).itemsDetailData = resInfo_detail
            }else{
                //not view
            }
        }else{
            Toast.makeText(this,"no data", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

}//class end