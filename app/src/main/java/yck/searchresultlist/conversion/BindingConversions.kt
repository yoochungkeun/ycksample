package yck.searchresultlist.conversion

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import yck.searchresultlist.R
import java.text.DecimalFormat

object BindingConversions {

    @BindingAdapter("loadImage")
    @JvmStatic
    fun loadImage(imageView: ImageView, url:String) {
        Glide.with(imageView).load(url)
            .centerCrop()
            .error(R.mipmap.ic_launcher_round)
            .into(imageView)
    }

    @BindingAdapter("convertDecimalNum")
    @JvmStatic
    fun convertDecimalNum(tv_value:TextView,value:String){
        val decimalFormat = DecimalFormat("#,###")
        var intValue = value.toInt()
        tv_value.text = decimalFormat.format(intValue)
    }


}//obj end