package yck.searchresultlist.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import yck.searchresultlist.base.BaseConstant
import yck.searchresultlist.interfaces.IServiceAPI

class YCKClient<T> {

    lateinit var mClient: Retrofit
    private var mService:T? = null

    fun getClient():T{
        if(mService==null){
            mClient = Retrofit.Builder()
                .baseUrl(BaseConstant.BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
            mService = mClient.create(IServiceAPI::class.java) as (T)
        }
        return mService!!
    }


}
