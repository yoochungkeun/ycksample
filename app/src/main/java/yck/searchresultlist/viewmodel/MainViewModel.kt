package yck.searchresultlist.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import yck.searchresultlist.base.BaseTAG
import yck.searchresultlist.base.BaseViewModel
import yck.searchresultlist.model.ResItemsData

class MainViewModel(context: Context):BaseViewModel() {

    val mLoginLiveData = MutableLiveData<ResItemsData>()

    fun getJsonInfoData() {
        var que = getServiceAPI().onTestData()
        que.enqueue(object : Callback<ResItemsData> {
            override fun onFailure(p0: Call<ResItemsData>?, throwable: Throwable?) {
                Log.d(BaseTAG.LOG_MAINVIEWMODEL,"getJsonInfoData()/onFailure")
            }
            override fun onResponse(call: Call<ResItemsData>?, response: Response<ResItemsData>?) {
                if (response!!.isSuccessful) {
                    var data = response.body()
                    Log.d(BaseTAG.LOG_MAINVIEWMODEL,"getJsonInfoData()/onResponse/data : ${data}")
                    mLoginLiveData.value = data
                } else {
                    Log.d(BaseTAG.LOG_MAINVIEWMODEL,"getJsonInfoData()/onResponse/failed")
                }
            }
        })
    }


}//class end