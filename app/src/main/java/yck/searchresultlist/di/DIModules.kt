package yck.searchresultlist.di

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import yck.searchresultlist.utils.CommonUtil

//Util
val commonutilModule = module {
    single { CommonUtil(androidApplication()) }
}

//=========================================
var customDImodules = listOf(
    commonutilModule
)


