package yck.searchresultlist.di

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import yck.searchresultlist.utils.CommonUtil

object DIManager:KoinComponent {

    //inject
    val commonUtil: CommonUtil by inject()

}//class end


