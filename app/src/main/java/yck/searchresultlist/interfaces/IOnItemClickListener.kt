package yck.searchresultlist.interfaces

import yck.searchresultlist.model.ResItemsDetailData

interface IOnItemClickListener{
    fun onItemClick(resInfo_detail: ResItemsDetailData)
}
