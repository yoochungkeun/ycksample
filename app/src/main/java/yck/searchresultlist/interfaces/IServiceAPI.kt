package yck.searchresultlist.interfaces

import retrofit2.Call
import retrofit2.http.*
import yck.searchresultlist.model.ResItemsData

interface IServiceAPI {

    @GET("test_data.json")
    fun onTestData(): Call<ResItemsData>


}//class end