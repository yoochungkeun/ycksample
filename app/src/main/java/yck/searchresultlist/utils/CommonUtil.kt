package yck.searchresultlist.utils

import android.content.Context
import androidx.room.TypeConverter
import com.google.gson.Gson
import yck.searchresultlist.model.ResItemsData


class CommonUtil(context: Context){

    private var mContext = context

    fun isEmptyString(source:String?):Boolean{
        if(source==null){
            return true
        }
        var trim_source = source.trim()
        if(trim_source.isEmpty() || trim_source.toLowerCase().equals("null") ||  trim_source.length<=0){
            return true
        }
        return false
    }

    fun jsonConverter(){
        @TypeConverter
        fun listToJson(value: ArrayList<ResItemsData>?) = Gson().toJson(value)
        @TypeConverter
        fun jsonToList(value: String) = Gson().fromJson(value, Array<ResItemsData>::class.java).toList()
    }

}//class end